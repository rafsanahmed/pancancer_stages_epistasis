import numpy as np
from tqdm import tqdm, trange
from sklearn.linear_model import LogisticRegression
import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import os
from utils import *


def mutex_nsep_all(gene_vs_patients_file, G, threshold = 0.95):
    with open(gene_vs_patients_file, 'r') as f:
        gene_patient_dict = {}
        for line in f.readlines():
            line = line.split()
            gene_patient_dict[line[0]] = line[1:]

    d = {}
    d2 = {}
    d3 = {}
    #cosmic genes
    for g1 in tqdm(cosmic_genes):

        if g1 in G.nodes():
            count = 0
            sum_in = 0
            list_vals = []
            for g2 in G.nodes():

                if g1 in gene_patient_dict and g2 in gene_patient_dict:

                    g1_neighbors = G[g1]
                    g2_neighbors = G[g2]

                    g1_patients = gene_patient_dict[g1]
                    g2_patients = gene_patient_dict[g2]

                    all_patients_g1 = g1_patients[:]
                    for gene in g1_neighbors:
                        if gene in gene_patient_dict and gene != g2:
                            all_patients_g1.extend(gene_patient_dict[gene])

                    all_patients_g2 = g2_patients[:]
                    for gene in g2_neighbors:
                        if gene in gene_patient_dict and gene != g1:
                            all_patients_g2.extend(gene_patient_dict[gene])

                    u1 = float(len(set(all_patients_g1))) / float(len(all_patients_g1))
                    u2 = float(len(set(all_patients_g2))) / float(len(all_patients_g2))

                    mutex = (u1 + u2) / 2.0
                    count += 1
                    sum_in += mutex
                    list_vals.append(mutex)

            if count!= 0 and sum !=0:
                d[g1] = sum_in/float(count)
                d3[g1] = np.median(list_vals)
                l = [v for v in list_vals if v>threshold]
                if len(l)!=0:
                    # if g1 == 'CREB1':
                    #     print len(l), len(list_vals)

                    d2[g1] = float(len(l))/float(len(list_vals))

    print len(d2)
    return (d,d2,d3)

def mutex_nsep_network_edges(gene_vs_patients_file, G,threshold=0.95):
    with open(gene_vs_patients_file, 'r') as f:
        gene_patient_dict = {}
        for line in f.readlines():
            line = line.split()
            gene_patient_dict[line[0]] = line[1:]

    d = {}
    d2 = {}
    d3 = {}
    edges = G.edges()
    #cosmic genes
    for g1 in tqdm(cosmic_genes):

        if g1 in G.nodes():
            count = 0
            sum_in = 0
            list_vals = []
            for g2 in G.nodes():

                if g1 in gene_patient_dict and g2 in gene_patient_dict:

                    if (g1,g2) is edges or (g2,g1) in edges:

                        g1_neighbors = G[g1]
                        g2_neighbors = G[g2]

                        g1_patients = gene_patient_dict[g1]
                        g2_patients = gene_patient_dict[g2]

                        all_patients_g1 = g1_patients[:]
                        for gene in g1_neighbors:
                            if gene in gene_patient_dict and gene != g2:
                                all_patients_g1.extend(gene_patient_dict[gene])

                        all_patients_g2 = g2_patients[:]
                        for gene in g2_neighbors:
                            if gene in gene_patient_dict and gene != g1:
                                all_patients_g2.extend(gene_patient_dict[gene])

                        u1 = float(len(set(all_patients_g1))) / float(len(all_patients_g1))
                        u2 = float(len(set(all_patients_g2))) / float(len(all_patients_g2))

                        mutex = (u1 + u2) / 2.0
                        count += 1
                        sum_in += mutex
                        list_vals.append(mutex)

            if count!= 0 and sum !=0:
                d[g1] = sum_in/float(count)
                d3[g1] = np.median(list_vals)
                l = [v for v in list_vals if v > threshold]
                if len(l) != 0:
                    # if g1 == 'CREB1':
                    #     print len(l), len(list_vals)

                    d2[g1] = float(len(l)) / float(len(list_vals))
    print d2
    print d3
    return (d,d2,d3)

def coverage(gene_vs_patients_file, G):
    ns = set()
    with open(gene_vs_patients_file, 'r') as f:
        gene_patient_dict = {}
        for line in f.readlines():
            line = line.split()
            gene_patient_dict[line[0]] = line[1:]
            ns.update(line[1:])
    print len(ns)
    num_samples = float(len(ns))

    d = {}
    #cosmic genes
    for g1 in tqdm(cosmic_genes):

        if g1 in G.nodes() and g1 in gene_patient_dict:

            d[g1] = float(len(gene_patient_dict[g1]))/num_samples

    return d

def get_same_genes_list(A,B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    print 'set', len(s)

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)

def load_edge_list_genes(infile):
    with open(infile, 'r') as f:
        edge_list = [
            [id_to_gene[int(line.split()[0]) - GENE_ID_OFFSET], id_to_gene[int(line.split()[1]) - GENE_ID_OFFSET]] for
            line in f.readlines()]
        return edge_list

def plot_save_figs(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    plt.scatter(x, y, alpha=0.5)
    for i, txt in enumerate(genes):
        if y[i] < t:
            plt.annotate(txt, (x[i], y[i]), fontsize=2)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(filepath, format = 'pdf', bbox_inches="tight", dpi = 800)
    plt.close()


if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']


    inpath = '../data/subtypes/'
    cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'
    hint_index_file = '../data/hint_index_file.txt'
    hint_edge_file = '../data/hint_edge_file.txt'

    with open(cosmic_file,'r') as f:
        cosmic_genes = [line.split()[0].upper() for line in f.readlines()[1:]]

    id_to_gene = load_id_to_gene()
    gene_to_id = load_gene_to_id()
    edge_list = load_edge_list_genes(hint_edge_file)  # PPI EDGE LIST

    # create graph
    G = nx.Graph()
    for e in edge_list:
        G.add_edge(e[0], e[1])

    G.to_undirected()
    G.remove_edges_from(G.selfloop_edges())
    print len(G.nodes)

    for c in cancer_subtypes:

        if c == 'BRCA':

            MLA_infile = inpath + c +'/' + c + '_MLA_standardized.txt'
            gene_vs_patients_infile = inpath + c +'/' + c + '.txt'
            plotpath = '../out/figures/' + c + '/'
            if not os.path.exists(plotpath):
                os.makedirs(plotpath)

            with open(MLA_infile, 'r') as f:

                MLA = {line.split()[0]:float(line.split()[1]) for line in f.readlines()}
                print 'MLA',len(MLA)
                mutex_dict, mutex_dict_perc_sigf, mutex_dict_median =  mutex_nsep_all(gene_vs_patients_infile, G)
                print 'Mutex_all',len(mutex_dict)

                mutex_dict_ppi,mutex_dict_perc_sigf_ppi, mutex_dict_median_ppi = mutex_nsep_network_edges(gene_vs_patients_infile, G)
                print 'Mutex_ppi',len(mutex_dict_ppi)

                cov_dict = coverage(gene_vs_patients_infile, G)
                print 'cov',len(cov_dict)



                # plot weights for all neighbor genes
                plot_save_figs(plotpath + c + '_MLA_vs_mutex_perc_sigf.pdf',MLA, mutex_dict_perc_sigf, t=0.8,
                               title='MLA vs Mutex Percentage significance', xlabel='MLA', ylabel='Mutex percentage (0.95)')

                cov_vals, mutex_median_vals, genes = get_same_genes_list(cov_dict, mutex_dict_median)
                weights = {genes[i]: cov_vals[i] * mutex_median_vals[i] for i in range(len(genes))}
                plot_save_figs(plotpath + c + '_MLA_vs_edge_weights.pdf', MLA, weights, t=0.8,
                               title='MLA vs edge weights', xlabel='MLA',
                               ylabel='cov * mutex(median)')

                # plot weights for all genes in network
                plot_save_figs(plotpath + c + '_MLA_vs_mutex_perc_sigf_ppi.pdf', MLA, mutex_dict_perc_sigf_ppi, t=0.8,
                               title='MLA vs Mutex Percentage significance in network', xlabel='MLA',
                               ylabel='Mutex percentage(0.95)')

                cov_vals2, mutex_median_vals2, genes2 = get_same_genes_list(cov_dict, mutex_dict_median_ppi)
                weights2 = {genes2[i]: cov_vals2[i] * mutex_median_vals2[i] for i in range(len(genes2))}
                plot_save_figs(plotpath + c + '_MLA_vs_edge_weights_ppi.pdf', MLA, weights2, t=0.8,
                               title='MLA vs edge weights in network', xlabel='MLA',
                               ylabel='cov * mutex(median)')

