import numpy as np
from tqdm import tqdm, trange
import pandas as pd
import matplotlib.pyplot as plt

def get_same_genes_list(A,B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    print 'set', len(s)

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)

def plot_save_figs(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    plt.scatter(x, y, alpha=0.5)
    for i, txt in enumerate(genes):
        if y[i] < t:
            plt.annotate(txt, (x[i], y[i]), fontsize=2)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)



if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']

    inpath = '../data/subtypes/'

    for c in cancer_subtypes:
        if c == 'BRCA':

            plotpath = '../out/figures/' + c + '/'
            mut_freq_file = inpath + c + '/' + c + '_freq.txt'
            tml_file = inpath + c + '/' + c + '_patient_vs_genes_binary.txt'

            with open(mut_freq_file, 'r') as f:
                mut_freq = {line.split()[0]: float(line.split()[1]) for line in f.readlines()}

            df = pd.read_csv(tml_file, sep='\t', header=0,skipinitialspace=True,usecols=['Patients','y'])
            print df.head()

            patients = df['Patients'].tolist()
            y_vals = df['y'].tolist()
            y_vals = [int(yy) for yy in y_vals]
            print patients
            print y_vals
            quit()
            # print 'freq',mut_freq
            # print 'MLA', mla
            genes = sorted(set.intersection(set(mut_freq), set(MLA)))

            plot_save_figs(plotpath + c + '_MLA_vs_mut_freq.pdf', MLA, mut_freq, t=0.999,
                           title='MLA vs mut_freq', xlabel='MLA',
                           ylabel='mut_freq')