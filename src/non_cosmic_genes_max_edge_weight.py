import numpy as np
from tqdm import tqdm, trange
import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import glob
import math
from utils import *

def get_data (gene_vs_patients_file):
    ns = set()

    with open(gene_vs_patients_file, 'r') as f:
        gene_patient_dict = {}
        for line in f.readlines():
            line = line.split()
            g = line[0].upper()
            if g in gene_patient_dict:
                for p in line[1:]:
                    gene_patient_dict[g].append(p)
            else:
                gene_patient_dict[g] = line[1:]

            ns.update(line[1:])
    # print len(ns)
    num_samples = float(len(ns))

    return (gene_patient_dict, ns)


def compute_mutex_nsep(G, gene_patient_dict):

    d = {}

    for e in G.edges():

        g1 = e[0]
        g2 = e[1]

        if g1 in gene_patient_dict and g2 in gene_patient_dict:

            g1_neighbors = G[g1]
            g2_neighbors = G[g2]

            g1_patients = gene_patient_dict[g1]
            g2_patients = gene_patient_dict[g2]

            all_patients_g1 = g1_patients[:]
            for gene in g1_neighbors:
                if gene in gene_patient_dict and gene != g2:
                    all_patients_g1.extend(gene_patient_dict[gene])

            all_patients_g2 = g2_patients[:]
            for gene in g2_neighbors:
                if gene in gene_patient_dict and gene != g1:
                    all_patients_g2.extend(gene_patient_dict[gene])

            u1 = float(len(set(all_patients_g1))) / float(len(all_patients_g1))
            u2 = float(len(set(all_patients_g2))) / float(len(all_patients_g2))

            mutex = (u1 + u2) / 2.0

            d[g1+' '+g2] = mutex

    return d


def load_edge_list_genes(infile):
    with open(infile, 'r') as f:
        edge_list = [
            [id_to_gene[int(line.split()[0]) - GENE_ID_OFFSET], id_to_gene[int(line.split()[1]) - GENE_ID_OFFSET]] for
            line in f.readlines()]
        return edge_list

def coverage(gene_patient_dict, ns):

    num_samples = float(len(ns))

    d = {}
    #cosmic genes
    for g1 in gene_patient_dict:

        if g1 in gene_patient_dict:

            d[g1] = float(len(gene_patient_dict[g1]))/num_samples

    return d

def get_same_genes_list(A,B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    # print 'set', len(s)

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)

def plot_save_figs(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    plt.scatter(x, y, alpha=0.5)
    for i, txt in enumerate(genes):
        if y[i] < t:
            plt.annotate(txt, (x[i], y[i]), fontsize=2)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)

def plot_save_figs_cosmic(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)

    cx = []
    cy = []
    ncx = []
    ncy = []
    for i, txt in enumerate(genes):
        if txt in cosmic_genes:
            cx.append(x[i])
            cy.append(y[i])
            # plt.scatter(x[i], y[i], c='C1', alpha=0.5,s=4)
        else:
            ncx.append(x[i])
            ncy.append(y[i])
            # plt.scatter(x[i], y[i], c='C0', alpha=0.5,s=4)
    plt.scatter(ncx, ncy, c='C0', alpha=0.25, s=4)
    plt.scatter(cx, cy, c='C1', alpha=0.4, s=6)


    for i, txt in enumerate(genes):
        if y[i] > t:
            plt.annotate(txt, (x[i], y[i]), fontsize=0.1)

    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)

def plot_hist (filepath,dict1, dict2,title='',xlabel='',ylabel=''):
    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    cg = []
    ncg = []

    for i in range(len(x)):

        if genes[i] in cosmic_genes:
            cg.append(x[i])
        else:
            ncg.append(x[i])

    plt.hist(cg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.3, color='C1')
    plt.hist(ncg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.3, color='C0')
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)

if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC', 'pancancer']
    inpath = '../data/subtypes/'
    num_genes = 500

    for c in tqdm(cancer_subtypes):
        if c != 'LAML':
            plotpath = '../out/figures/' + c + '/'
            cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'
            mla_file = inpath + c + '/' + c + '_MLA_standardized.txt'
            gene_vs_patients_infile = inpath + c +'/' + c + '.txt'

            hint_index_file = '../data/hint_index_file.txt'
            hint_edge_file = '../data/hint_edge_file.txt'

            outfile = plotpath + 'max_gene_weights_details_n' + str(num_genes) + '.txt'

            if c == 'pancancer':
                gene_nfile = glob.glob('../out/connected_components_isolarge/mexcowalk_orig/cc_n' + str(num_genes) + '_*.txt')[0]
                edge_weight_file = '../out/edge_weights/mexcowalk_orig.txt'
            else:
                gene_nfile = glob.glob('../out/connected_components_isolarge/mexcowalk_' + c + '/cc_n' + str(num_genes) + '_*.txt')[0]
                edge_weight_file = '../out/edge_weights/' + c + '_mexcowalk.txt'

            with open(cosmic_file, 'r') as f:
                cosmic_genes = [line.split()[0] for line in f.readlines()[1:]]

            with open(mla_file, 'r') as f:
                MLA = {line.split()[0]: float(line.split()[1]) for line in f.readlines()}

            with open(gene_nfile, 'r') as f:
                mexcowalk_genes = [v.upper() for line in f.readlines() for v in line.split()]

            with open(edge_weight_file, 'r') as f:
                mexcowalk_edges = {(line.split()[0].upper(), line.split()[1].upper()):float(line.split()[2]) for line in f.readlines()}

            #non cosmic genes
            nc_genes = set(mexcowalk_genes) - set.intersection(set(mexcowalk_genes), set(cosmic_genes))


            id_to_gene = load_id_to_gene()
            data, ns = get_data(gene_vs_patients_infile)
            gene_to_id = load_gene_to_id()
            edge_list = load_edge_list_genes(hint_edge_file)  # PPI EDGE LIST
            coverage_dict = coverage(data,ns)

            # create graph
            G = nx.Graph()
            for e in mexcowalk_edges:
                # print mexcowalk_edges[e]
                G.add_edge(e[0].upper(), e[1].upper(), weight=mexcowalk_edges[e])

            G.to_undirected()
            G.remove_edges_from(G.selfloop_edges())

            mutex_nsep_dict = compute_mutex_nsep(G, data)

            print len(G.nodes)
            print len(G.edges())

            #d contains weights, d2 contains neighbor genes
            d = {}
            d2 = {}
            for g in mexcowalk_genes:
                g=g.upper()
                all_edges = sorted(G.edges(g, data=True), key=lambda x:x[2]['weight'], reverse=True)
                val = all_edges[0]

                d[val[0]] = val[2]['weight']
                d2[val[0]] = val[1]

            # print d
            count = 0
            for g in d2:
                if d2[g] in cosmic_genes:
                    # print g, d2[g]
                    count+=1
            print count

            # print d
            plot_save_figs_cosmic(plotpath + c + '_MLA_vs_max_edge_weights_cosmic_n' + str(num_genes) + '.pdf', MLA, d, t=1,
                           title='MLA vs max edge weights', xlabel='MLA',
                           ylabel='mutex_nsep *cov *cov')

            ## if required, plot MLA histogram
            # plot_hist(plotpath + c + '_MLA_vs_max_edge_weights_cosmic_n' + str(num_genes) + '_hist.pdf',MLA,d,
            #           title='MLA max distribution', xlabel='MLA',
            #           ylabel='counts'
            #           )

            # print 'inters', len(d), len(coverage_dict),len(set.intersection(set(d), set(coverage_dict)))
            with open(outfile, 'w') as f:
                f.write('gene\tweight\tcoverage\tneighbor\tcosmic\tneighbor_cov\tmutex\tMLA_gene\tMLA_neighbor\n')

                for g in d:
                    f.write(g + '\t' + str(d[g]) + '\t') #+
                    f.write(str(coverage_dict[g])+'\t' + d2[g])
                    if d2[g] in cosmic_genes:
                        f.write('\tT')
                    else:
                        f.write('\tF')

                    f.write( '\t' + str(coverage_dict[d2[g]]) +'\t')
                    try:
                        f.write(str(mutex_nsep_dict[g+' '+ d2[g]]) + '\n')
                        m=mutex_nsep_dict[g+' '+ d2[g]]
                    except:
                        f.write(str(mutex_nsep_dict[d2[g] + ' ' + g]) + '\t')
                        f.write(str(MLA[g]) + '\t' + str(MLA[d2[g]]) + '\n')
                        m = mutex_nsep_dict[d2[g] + ' ' + g]

                    # print g, m*coverage_dict[g]*coverage_dict[d2[g]]


            print  c, 'MAXMIN MLA', min(MLA.values()),max(MLA.values())



