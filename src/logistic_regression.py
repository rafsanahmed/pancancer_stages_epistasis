import numpy as np
from tqdm import tqdm, trange
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
import pandas as pd
from statsmodels.discrete.discrete_model import Logit

cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']


inpath = '../data/subtypes/'

for c in cancer_subtypes[1:4]:
    #c = cancer_subtypes[0]
    if c =='BRCA':
        infile = inpath + c+ '/' + c + '_patient_vs_genes_binary.txt'

        df = pd.read_csv(infile, sep='\t', index_col=0, header=0)
        d = {}
        # print df.head()

        # for index, rows in df.iterrows():
        #     print index, sum(rows)
        count = 0
        for cc in df:
            if cc != 'y':
                if sum(df[cc])>20:
                    count +=1
        print c, count

        print df.info()
        count = 0
        for cc in df:
            if cc != 'y':
                count += 1
                Y = np.asarray(df['y'])
                X = np.asarray(df[cc])
                # X = X.reshape(-1,1)
                # print 'XXXXXX',X

                logit = Logit(X,Y)
                res = logit.fit()
                # print 'res', res.summary()
                coef = res.params[0]
                d[cc] = coef
                # print 'res', res.params
                # logit = LogisticRegression()
                # logit.fit(X, Y)
                # print '?',logit.predict(X)
                # print '?',logit.predict(df['RNF10'])

                # if count > 4:
                #     break

        print d
        scaler = StandardScaler()
        print scaler.fit(d.values())