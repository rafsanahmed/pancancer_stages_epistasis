import numpy as np
from tqdm import tqdm, trange

cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']
# cancer_subtypes = ['pancancer']

inpath = '../data/subtypes/'

for c in cancer_subtypes[:]:
    # c = cancer_subtypes[0]
    outfile = inpath + c+ '/' + c + '_patient_vs_genes_binary.txt'
    indices_file = inpath + c+ '/' + c + '_patient_indices.txt'

    with open(indices_file, 'r') as f:
        patient_indices = {}
        for line in f.readlines():
            line = line.split()
            patient_indices[line[1]] = line[0]

    if 1:
        infile = inpath + c + '/' + c+ '.txt'
        gene_v_patients = {}
        patient_vs_genes = {}
        patient_vs_genes_binary = {}
        patient_set = set()


        with open(infile, 'r') as f:
            for line in f.readlines():
                line = line.split()
                if line[0].upper() in gene_v_patients:
                    print 'err'
                gene_v_patients[line[0].upper()] = line[1:]
                patient_set.update(line[1:])


        print len(gene_v_patients)
        print len(patient_set)

        gene_v_patients_list = gene_v_patients.keys()

        for p in patient_set:
            patient_vs_genes[p] = []
            patient_vs_genes_binary[p] = []

            #for genes
            for g in gene_v_patients_list:

                if p in gene_v_patients[g]:

                    patient_vs_genes_binary[p].append(1)
                    patient_vs_genes[p].append(g)

                else:
                    patient_vs_genes_binary[p].append(0)
                    patient_vs_genes[p].append(0)

        binary_sum = {}
        for k in patient_vs_genes_binary:
            binary_sum[k] = sum(patient_vs_genes_binary[k])
            # if binary_sum[k]<20:
            #     print k, binary_sum[k]
        # print binary_sum

        with open(outfile, 'w') as f:

            f.write('Patients')
            for g in gene_v_patients_list:
                f.write('\t' + g)
            f.write('\ty\n')

            for p in patient_vs_genes_binary:
                f.write(str(patient_indices[p]))
                for v in patient_vs_genes_binary[p]:
                    f.write('\t' + str(v))
                f.write('\t' + str(binary_sum[p]) + '\n')
