import numpy as np
from tqdm import tqdm, trange
from sklearn.linear_model import LogisticRegression
import pandas as pd
import matplotlib.pyplot as plt
import os

def get_same_genes_list(A,B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    print 'set', len(s)

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)

def plot_save_figs(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)

    cx = []
    cy = []
    ncx = []
    ncy = []
    for i, txt in tqdm(enumerate(genes)):
        if txt in cosmic_genes:
            cx.append(x[i])
            cy.append(y[i])
            # plt.scatter(x[i], y[i], c='C1', alpha=0.5,s=4)
        else:
            ncx.append(x[i])
            ncy.append(y[i])
            # plt.scatter(x[i], y[i], c='C0', alpha=0.5,s=4)
    plt.scatter(ncx, ncy, c='C0', alpha=0.25, s=4)
    plt.scatter(cx, cy, c='C1', alpha=0.4, s=6)


    for i, txt in tqdm(enumerate(genes)):
        if y[i] > t:
            plt.annotate(txt, (x[i], y[i]), fontsize=0.1)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)

def get_patient_vs_genes(data):
    d = {}

    for g in data:

        for p in data[g]:

            if p in d:
                d[p].append(g)
            else:
                d[p] = [g]
    return d

if __name__ == '__main__':

    # cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']

    cancer_subtypes = ['pancancer']
    inpath = '../data/subtypes/'



    for c in cancer_subtypes:

        if True:#'BRCA':

            infile = inpath + c + '/' + c + '.txt'
            outpath = '../out/figures/' + c + '/'

            if not os.path.exists(outpath):
                os.makedirs(outpath)

            with open(infile, 'r') as f:

                gene_vs_patients = {line.split()[0]: line.split()[1:] for line in f.readlines()}

            patient_vs_genes = get_patient_vs_genes(gene_vs_patients)

            tumor_mutation_load = {}
            for k,v in patient_vs_genes.iteritems():
                # print k,v
                tumor_mutation_load[k] = float(len(v))
            print len(tumor_mutation_load)
            plt.figure()
            plt.hist(tumor_mutation_load.values(), color='C0')
            # plt.show()
            plt.title(c+' mutation distribution per patient')
            plt.xlabel('mutations')
            plt.ylabel('Counts')
            plt.savefig(outpath +c+'_mutation_distribution_per_patient.pdf', format='pdf', bbox_inches='tight', dpi=800)

