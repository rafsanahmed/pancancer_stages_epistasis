import numpy as np
from tqdm import tqdm, trange
import pandas as pd
import matplotlib.pyplot as plt
import glob
import math

def get_same_genes_list(A,B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    print 'set', len(s)

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)

def plot_save_figs(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)

    cx = []
    cy = []
    ncx = []
    ncy = []
    for i, txt in tqdm(enumerate(genes)):
        if txt in cosmic_genes:
            cx.append(x[i])
            cy.append(y[i])
            # plt.scatter(x[i], y[i], c='C1', alpha=0.5,s=4)
        else:
            ncx.append(x[i])
            ncy.append(y[i])
            # plt.scatter(x[i], y[i], c='C0', alpha=0.5,s=4)
    plt.scatter(ncx, ncy, c='C0', alpha=0.25, s=4)
    plt.scatter(cx, cy, c='C1', alpha=0.4, s=6)


    for i, txt in tqdm(enumerate(genes)):
        if y[i] > t:
            plt.annotate(txt, (x[i], y[i]), fontsize=0.1)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()

def plot_hist (filepath,dict1, dict2,title='',xlabel='',ylabel=''):
    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    cg = []
    ncg = []

    for i in range(len(x)):

        if genes[i] in cosmic_genes:
            cg.append(x[i])
        else:
            ncg.append(x[i])

    plt.hist(cg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.3, color='C1')
    plt.hist(ncg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.3, color='C0')
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()

def plot_save_figs_with_all_cosmic(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)

    cx = []
    cy = []
    ncx = []
    ncy = []
    acgx = []
    acgy = []
    for i, txt in tqdm(enumerate(genes)):
        if txt in cosmic_genes:
            cx.append(x[i])
            cy.append(y[i])
            # plt.scatter(x[i], y[i], c='C1', alpha=0.5,s=4)
        else:
            ncx.append(x[i])
            ncy.append(y[i])
            # plt.scatter(x[i], y[i], c='C0', alpha=0.5,s=4)
    count = 0
    for i, txt in enumerate(cosmic_genes):
        if txt not in genes and txt in MLA and txt in mut_freq:
            acgx.append(MLA[txt])
            acgy.append(mut_freq[txt])
            count +=1

    plt.scatter(ncx, ncy, c='C0', alpha=0.25, s=4)
    plt.scatter(cx, cy, c='C1', alpha=0.4, s=6)
    plt.scatter(acgx, acgy, c='C3', alpha=0.2, s=6)

    print count
    for i, txt in tqdm(enumerate(genes)):
        if y[i] > t:
            plt.annotate(txt, (x[i], y[i]), fontsize=0.1)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()

def plot_hist_all_cosmic (filepath,dict1, dict2,title='',xlabel='',ylabel=''):
    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    cg = []
    ncg = []
    acg = []
    colors = ['C3', 'C1','C0']
    lables = ['remaining_cosmic', 'module_cosmic', 'non_cosmic']
    for i in range(len(x)):

        if genes[i] in cosmic_genes:
            cg.append(x[i])
        else:
            ncg.append(x[i])

    count = 0
    for i, txt in enumerate(cosmic_genes):
        if txt not in genes and txt in MLA and txt in mut_freq:
            acg.append(MLA[txt])
            count +=1
    print 'count',count

    x = MLA.values()
    plt.hist([acg,cg,ncg], bins=range(int(math.floor(min(x))), int(math.ceil(max(x))) + 1), alpha=0.5, color=colors)
    # plt.hist(cg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.5, color='C1')
    # plt.hist(ncg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.5, color='C0')

    plt.legend(lables)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()


if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC', 'pancancer']
    # cancer_subtypes = ['pancancer']
    inpath = '../data/subtypes/'
    cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'

    num_genes = 200

    with open(cosmic_file,'r') as f:
        cosmic_genes = [line.split()[0].upper() for line in f.readlines()[1:]]

    for c in cancer_subtypes:
        if 1:#c!= 'LAML':

            plotpath = '../out/figures/' + c + '/'
            mut_freq_file = inpath + c + '/' + c + '_freq.txt'
            mla_file = inpath + c + '/' + c + '_MLA_standardized.txt'

            if c == 'pancancer':
                gene_nfile = glob.glob('../out/connected_components_isolarge/mexcowalk_orig/cc_n' + str(num_genes) + '_*.txt')[0]
            else:
                gene_nfile = glob.glob('../out/connected_components_isolarge/mexcowalk_' + c + '/cc_n' + str(num_genes) + '_*.txt')[0]

            with open(gene_nfile, 'r') as f:
                mexcowalk_genes = [v for line in f.readlines() for v in line.split()]

            with open(mut_freq_file, 'r') as f:
                mut_freq = {line.split()[0]: float(line.split()[1]) for line in f.readlines()}

            with open(mla_file, 'r') as f:
                MLA = {line.split()[0]: float(line.split()[1]) for line in f.readlines()}

            # print 'freq',mut_freq
            # print 'MLA', mla
            # genes = sorted(set.intersection(set(mut_freq), set(MLA)))
            MLA2 = {k:v for k,v in MLA.iteritems() if k in mexcowalk_genes}
            mut_freq2 = {k:v for k,v in mut_freq.iteritems() if k in mexcowalk_genes}

            #plots
            # plot_save_figs(plotpath + c + '_MLA_vs_mut_freq_' + str(num_genes) + '.pdf', MLA2, mut_freq2, t=0.001,
            #                title='MLA vs mut_freq', xlabel='MLA',
            #                ylabel='mut_freq')

            ## scatter plot mutation freq * MLA with all cosmic genes
            plot_save_figs_with_all_cosmic(plotpath + c + '_MLA_vs_mut_freq_all_cosmic_' + str(num_genes) + '.pdf', MLA2, mut_freq2, t=0.001,
                           title='MLA vs mut_freq', xlabel='MLA',
                           ylabel='mut_freq')

            #hist
            ##plot hist among two dicts
            # plot_hist(plotpath + c + '_MLA_hist_' + str(num_genes) + '.pdf', MLA2, mut_freq2,
            #                title='MLA histogram', xlabel='MLA',
            #                ylabel='counts')


            ## main hist plot to compare module cosmic, non cosmic and remaining cosmic in the cosmic file
            plot_hist_all_cosmic(plotpath + c + '_MLA_hist_all_cosmic_' + str(num_genes) + '.pdf', MLA2, mut_freq2,
                           title=c+' MLA histogram all cosmic', xlabel='MLA',
                           ylabel='counts')

