import numpy as np
from tqdm import tqdm, trange
import pandas as pd
import matplotlib.pyplot as plt
import glob
import math

def get_same_genes_list(A,B):
    k1 = A.keys()
    k2 = B.keys()
    s = set.intersection(set(k1), set(k2))
    print 'set', len(s)

    list_A = []
    list_B = []
    genes = []
    for g in sorted(s):
        list_A.append(A[g])
        list_B.append(B[g])
        genes.append(g)

    return (list_A, list_B, genes)

def plot_save_figs(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)

    cx = []
    cy = []
    ncx = []
    ncy = []
    for i, txt in tqdm(enumerate(genes)):
        if txt in cosmic_genes:
            cx.append(x[i])
            cy.append(y[i])
            # plt.scatter(x[i], y[i], c='C1', alpha=0.5,s=4)
        else:
            ncx.append(x[i])
            ncy.append(y[i])
            # plt.scatter(x[i], y[i], c='C0', alpha=0.5,s=4)
    plt.scatter(ncx, ncy, c='C0', alpha=0.25, s=4)
    plt.scatter(cx, cy, c='C1', alpha=0.4, s=6)


    for i, txt in tqdm(enumerate(genes)):
        if y[i] > t:
            plt.annotate(txt, (x[i], y[i]), fontsize=0.1)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()

def plot_hist (filepath,dict1, dict2,title='',xlabel='',ylabel=''):
    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)
    cg = []
    ncg = []

    for i in range(len(x)):

        if genes[i] in cosmic_genes:
            cg.append(x[i])
        else:
            ncg.append(x[i])

    plt.hist(cg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.3, color='C1')
    plt.hist(ncg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x)))+1), alpha=0.3, color='C0')
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()


def plot_hist_all(filepath, title='', xlabel='', ylabel='',step=0.25):
    plt.figure()
    # x, y, genes = get_same_genes_list(dict1, dict2)
    mla_min = int(math.floor((min(MLA.values()))))
    mla_max = int(math.ceil(max(MLA.values())))

    r = np.arange(mla_min, mla_max + step, step)
    ticks = np.arange(mla_min,mla_max+1)

    cg = []
    ncg = []

    colors = ['C1', 'C0']

    for g in MLA:

        if g in cosmic_genes:
            cg.append(MLA[g])
        else:
            ncg.append(MLA[g])

    x = MLA.values()
    plt.hist([cg,ncg], bins=r, alpha=0.3, color=colors)
    # plt.hist(ncg, bins=range(int(math.floor(min(x))), int(math.ceil(max(x))) + 1), alpha=0.3, color='C0')
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xticks(ticks)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()

def plot_save_figs_with_all_cosmic(filepath, dict1, dict2, t=0.90,title='',xlabel='',ylabel=''):

    plt.figure()
    x, y, genes = get_same_genes_list(dict1, dict2)

    cx = []
    cy = []
    ncx = []
    ncy = []
    acgx = []
    acgy = []
    for i, txt in tqdm(enumerate(genes)):
        if txt in cosmic_genes:
            cx.append(x[i])
            cy.append(y[i])
            # plt.scatter(x[i], y[i], c='C1', alpha=0.5,s=4)
        else:
            ncx.append(x[i])
            ncy.append(y[i])
            # plt.scatter(x[i], y[i], c='C0', alpha=0.5,s=4)
    count = 0
    for i, txt in enumerate(cosmic_genes):
        if txt not in genes and txt in MLA and txt in mut_freq:
            acgx.append(MLA[txt])
            acgy.append(mut_freq[txt])
            count +=1

    plt.scatter(ncx, ncy, c='C0', alpha=0.25, s=4)
    plt.scatter(cx, cy, c='C1', alpha=0.4, s=6)
    plt.scatter(acgx, acgy, c='C2', alpha=0.4, s=6)

    print count
    for i, txt in tqdm(enumerate(genes)):
        if y[i] > t:
            plt.annotate(txt, (x[i], y[i]), fontsize=0.1)
    plt.grid()
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.ylim(-0.001,0.006)
    plt.savefig(filepath, format='pdf', bbox_inches="tight", dpi=800)
    plt.close()

if __name__ == '__main__':

    cancer_subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC', 'pancancer']
    # cancer_subtypes = ['pancancer']
    inpath = '../data/subtypes/'
    outpath = '../out/figures/'
    cosmic_file = '../data/Census_allFri_Apr_26_12_49_57_2019.tsv'

    num_genes = 100

    with open(cosmic_file,'r') as f:
        cosmic_genes = [line.split()[0].upper() for line in f.readlines()[1:]]

    for c in tqdm(cancer_subtypes):
        if 1:#c== 'BRCA':

            plotpath = '../out/figures/' + c + '/'
            mut_freq_file = inpath + c + '/' + c + '_freq.txt'
            mla_file = inpath + c + '/' + c + '_MLA_standardized.txt'

            with open(mla_file, 'r') as f:
                MLA = {line.split()[0]:float(line.split()[1]) for line in f.readlines()}

            plot_hist_all(plotpath + c + '_MLA_hist_all_b0.25.pdf',
                           title=c+' all genes MLA histogram', xlabel='MLA',
                           ylabel='counts', step=0.25)